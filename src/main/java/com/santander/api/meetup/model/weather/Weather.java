package com.santander.api.meetup.model.weather;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Weather implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Day> list;

}
