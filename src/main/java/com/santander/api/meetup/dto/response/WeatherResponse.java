package com.santander.api.meetup.dto.response;

import java.time.Instant;

import com.santander.api.meetup.model.weather.Day;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class WeatherResponse extends SuccessResponse {

	private Instant dt;

	private Double min;

	private Double max;

	public WeatherResponse(Day day) {
		this(day.getDt(), day.getTemp().getMin(), day.getTemp().getMax());
	}

	public WeatherResponse(Day day, String message) {
		this(day.getDt(), day.getTemp().getMin(), day.getTemp().getMax());
		addMessage(message);
	}

}
