package com.santander.api.meetup.dto;

import com.santander.api.meetup.model.Meetup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MeetupProvisionDTO {

	Meetup meetup;

	double cantidadAComprar;

}
