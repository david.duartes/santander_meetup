package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.santander.api.meetup.facade.MeetupInfoFacade;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class MeetupInfoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	@Getter
	private MeetupInfoFacade meetupInfoFacade;

//	@Autowired
//	@Getter
//	private RestTemplate restTemplate;

//	@BeforeEach
//  void setMockOutput() {
//      when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//  }

//	@Test
	public void obtenerTemperaturaTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/meetups/" + id + "/daily/info").header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzYW50YW5kZXJKV1QiLCJzdWIiOiJhZG1pbiIsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJpYXQiOjE1ODA4NTgwMzAsImV4cCI6MTU4MDg1ODYzMH0.LXKZnaXNdhGfOUb1TJjeCHi3gvxoLXeRym_tEY6cCM7UdbQR9M3Kz6lnfhOw6ZaiVCF6zcMlHvwZfJYj0bLz3w"))
				.andDo(print()).andExpect(status().isOk());
//				.andExpect(content().string(containsString("Hello, World")));
	}

//	@Test
	public void calcularProvisionTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/meetups/" + id + "/provisiones/info")).andDo(print()).andExpect(status().isOk());
//				.andExpect(content().string(containsString("Hello, World")));
	}

}
