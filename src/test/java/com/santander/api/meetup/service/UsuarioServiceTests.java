package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.UsuarioDao;
import com.santander.api.meetup.enums.TipoRolEnum;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.impl.UsuarioServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UsuarioServiceTests {

	private static final Rol rolPrueba = new Rol(1L, TipoRolEnum.ADMIN.name());

	private static final Usuario usuarioPrueba = new Usuario(1L, "prueba", "1234", rolPrueba, true);

	@Mock
	private UsuarioDao usuarioDao;

	@InjectMocks
	private UsuarioServiceImpl usuarioServiceImpl;

//	@BeforeEach
//    void setMockOutput() {
//        when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//    }

	@Test
	public void testSaveSuccess() {

		Usuario input = new Usuario(null, "prueba", "1234", rolPrueba, true);

		when(usuarioDao.save(input)).thenReturn(usuarioPrueba);

		assertEquals(usuarioPrueba, usuarioServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long idPrueba = 1L;

		Optional<Usuario> uOpResult = Optional.of(usuarioPrueba);

		when(usuarioDao.findById(idPrueba)).thenReturn(uOpResult);

		assertEquals(usuarioPrueba, usuarioServiceImpl.get(idPrueba));
	}

	@Test
	public void testFindByUserName() {

		String userName = "prueba";

		when(usuarioDao.findByUserName(userName)).thenReturn(usuarioPrueba);

		assertEquals(usuarioPrueba, usuarioServiceImpl.findByUserName(userName));
	}

}
