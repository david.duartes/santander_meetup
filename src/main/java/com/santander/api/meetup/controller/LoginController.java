package com.santander.api.meetup.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.LoginResponse;
import com.santander.api.meetup.model.Login;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.UsuarioService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
public class LoginController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping("login")
	@ApiOperation(value = "Obtener un token de acceso a los recursos")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Token generado correctamente", response = LoginResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Usuario inválido."),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Password inválida"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> login(
			@ApiParam(value = "Usuario con permisos para genera token", required = true, defaultValue = "admin") @RequestHeader("user") String username,
			@ApiParam(value = "Password del usuario", required = true, defaultValue = "admin") @RequestHeader("password") String pwd) {

		Usuario usuario = usuarioService.findByUserName(username);

		if (usuario == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Usuario inválido."), HttpStatus.NOT_FOUND);

		}

		if (!usuario.getPassword().equals(pwd)) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Password inválida"), HttpStatus.NOT_FOUND);

		}

		String token = getJWTToken(username);
		Login user = new Login();
		user.setUser(username);
		user.setToken(token);

		return new ResponseEntity<GenericResponse>(new LoginResponse(user, "Token generado correctamente"),
				HttpStatus.CREATED);

	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("santanderJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
