package com.santander.api.meetup.dto.response;

public class ErrorResponse extends GenericResponse {

	public ErrorResponse(String message) {
		super(false);
		addMessage(message);
	}

	public ErrorResponse() {
		this(null);
	}

}
