package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.ProvisionDao;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.impl.ProvisionServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ProvisionServiceTests {

	private static final Producto productoPrueba = new Producto(10L, "Pack cerveza", 6, 769.0);

	private static final Provision provisionPrueba = new Provision(10L, 20, productoPrueba);

	@Mock
	private ProvisionDao provisionDao;

	@InjectMocks
	private ProvisionServiceImpl provisionServiceImpl;

//	@BeforeEach
//    void setMockOutput() {
//        when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//    }

	@Test
	public void testSaveSuccess() {

		Provision input = new Provision(null, 20, productoPrueba);

		when(provisionDao.save(input)).thenReturn(provisionPrueba);

		assertEquals(provisionPrueba, provisionServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long idPrueba = 10L;

		Optional<Provision> uOpResult = Optional.of(provisionPrueba);

		when(provisionDao.findById(idPrueba)).thenReturn(uOpResult);

		assertEquals(provisionPrueba, provisionServiceImpl.get(idPrueba));
	}

}
