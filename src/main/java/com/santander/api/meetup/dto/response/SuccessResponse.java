package com.santander.api.meetup.dto.response;

public class SuccessResponse extends GenericResponse {

	public SuccessResponse(String message) {
		super(true);
		addMessage(message);
	}

	public SuccessResponse() {
		this(null);
	}

}
