package com.santander.api.meetup.service;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.model.Meetup;

public interface MeetupService extends GenericService<Meetup, Long> {

}
