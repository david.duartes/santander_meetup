package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.UsuarioResponse;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping
	@ApiOperation(value = "Crear usuario del sistema")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Usuario creado correctamente", response = UsuarioResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearUsuario(
			@ApiParam(value = "Datos del usuario a crear en el sistema", required = true) @Valid @RequestBody Usuario usuario) {

		Usuario usuarioCreated = usuarioService.save(usuario);

		return new ResponseEntity<GenericResponse>(new UsuarioResponse(usuarioCreated, "Usuario creado correctamente"),
				HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener usuario por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = UsuarioResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Usuario no encontrado"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getUsuarioById(@Valid @PathVariable("id") Long id) {
		Usuario usuario = usuarioService.get(id);

		if (usuario == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Usuario no encontrado"),
					HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<GenericResponse>(new UsuarioResponse(usuario, "Ok"), HttpStatus.OK);
	}

}
