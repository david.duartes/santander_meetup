package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.ProductoDao;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.service.ProductoService;

@Service
public class ProductoServiceImpl extends GenericServiceImpl<Producto, Long> implements ProductoService {

	@Autowired
	private ProductoDao productoDao;

	@Override
	public CrudRepository<Producto, Long> getDao() {
		return productoDao;
	}

}
