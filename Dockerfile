FROM maven:3.6.1-jdk-8 as santander-meetup

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY ./ .

RUN mvn install -DskipTests

FROM openjdk:8-jdk-alpine

ENV PROFILE dev

RUN mkdir -p /usr/src/app

COPY --from=santander-meetup /usr/src/app/target/*.jar  /usr/src/app/app.jar

WORKDIR /usr/src/app

#ENTRYPOINT ["java","-jar","app.jar","--Dspring.profiles.active=","echo $PROFILE"]

ENTRYPOINT ["java","-jar","app.jar"]
