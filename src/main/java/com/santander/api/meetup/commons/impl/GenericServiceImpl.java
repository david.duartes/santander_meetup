package com.santander.api.meetup.commons.impl;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.GenericService;

@Service
public abstract class GenericServiceImpl<T, ID extends Serializable> implements GenericService<T, ID> {

	@Override
	public T save(T entity) {
		return getDao().save(entity);
	}

	@Override
	public void delete(ID id) {
		getDao().deleteById(id);
	}

	@Override
	public T get(ID id) {
		Optional<T> objOptional = getDao().findById(id);
		if (objOptional.isPresent()) {
			return objOptional.get();
		}
		return null;
	}

	public abstract CrudRepository<T, ID> getDao();

}
