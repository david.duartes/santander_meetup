package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Suscripcion;
import com.santander.api.meetup.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SuscripcionResponse extends SuccessResponse {

	private Long id;

	private Usuario usuario;

	private Meetup meetup;

	public SuscripcionResponse(Suscripcion suscripcion) {
		this(suscripcion.getId(), suscripcion.getUsuario(), suscripcion.getMeetup());
	}

	public SuscripcionResponse(Suscripcion suscripcion, String message) {
		this(suscripcion.getId(), suscripcion.getUsuario(), suscripcion.getMeetup());
		addMessage(message);
	}

}
