package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Login;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LoginResponse extends SuccessResponse {

	private String user;

	private String token;

	public LoginResponse(Login login) {
		this(login.getUser(), login.getToken());
	}

	public LoginResponse(Login login, String message) {
		this(login.getUser(), login.getToken());
		addMessage(message);
	}

}
