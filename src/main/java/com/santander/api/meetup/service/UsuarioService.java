package com.santander.api.meetup.service;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.model.Usuario;

public interface UsuarioService extends GenericService<Usuario, Long> {

	public Usuario findByUserName(String userName);
	
}
