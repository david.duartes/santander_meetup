package com.santander.api.meetup.model.weather;

import java.io.Serializable;

import lombok.Data;

@Data
public class Temp implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double min;

	private Double max;

}
