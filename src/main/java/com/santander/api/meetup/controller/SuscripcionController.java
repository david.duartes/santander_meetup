package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.request.SuscripcionRequest;
import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.SuscripcionResponse;
import com.santander.api.meetup.exception.NotFoundMeetupException;
import com.santander.api.meetup.exception.NotFoundUsuarioException;
import com.santander.api.meetup.model.Suscripcion;
import com.santander.api.meetup.service.SuscripcionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/suscripciones")
public class SuscripcionController {

	@Autowired
	private SuscripcionService suscripcionService;

	@PostMapping
	@ApiOperation(value = "Suscribir un usuario a una meetup")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Suscripción exitosa", response = SuscripcionResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearSuscripcion(
			@ApiParam(value = "Datos de suscripción a una meetup", required = true) @Valid @RequestBody SuscripcionRequest suscripcion) {
		Suscripcion suscripcionCreated = null;
		try {
			suscripcionCreated = suscripcionService.crearSuscripcion(suscripcion);
		} catch (NotFoundMeetupException e) {
			return new ResponseEntity<GenericResponse>(new ErrorResponse("No se encontro meetup indicada"),
					HttpStatus.NOT_FOUND);
		} catch (NotFoundUsuarioException e) {
			return new ResponseEntity<GenericResponse>(new ErrorResponse("No se encontro usuario indicado"),
					HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<GenericResponse>(new SuscripcionResponse(suscripcionCreated, "Suscripción exitosa"),
				HttpStatus.CREATED);

	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener suscripción por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = SuscripcionResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getSuscripcionById(@Valid @PathVariable("id") Long id) {

		Suscripcion suscripcionCreated = suscripcionService.get(id);

		if (suscripcionCreated == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Suscripcion no encontrada"),
					HttpStatus.NOT_FOUND);

		}

		return new ResponseEntity<GenericResponse>(new SuscripcionResponse(suscripcionCreated, "Ok"), HttpStatus.OK);
	}
}
