package com.santander.api.meetup.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.santander.api.meetup.model.Rol;

@Repository
public interface RolDao extends CrudRepository<Rol, Long> {

}
