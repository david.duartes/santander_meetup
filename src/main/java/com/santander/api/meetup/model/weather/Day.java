package com.santander.api.meetup.model.weather;

import java.io.Serializable;
import java.time.Instant;

import lombok.Data;

@Data
public class Day implements Serializable {

	private static final long serialVersionUID = 1L;

	private Instant dt;

	private Temp temp;

}
