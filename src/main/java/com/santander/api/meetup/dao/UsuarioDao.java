package com.santander.api.meetup.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.santander.api.meetup.model.Usuario;

@Repository
public interface UsuarioDao extends CrudRepository<Usuario, Long> {

	Usuario findByUserName(String userName);

}
