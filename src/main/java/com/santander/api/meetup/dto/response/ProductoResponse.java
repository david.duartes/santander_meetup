package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Producto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProductoResponse extends SuccessResponse {

	private String nombre;

	private int cantidadXUnidad;

	private Double precio;

	public ProductoResponse(Producto producto) {
		this(producto.getNombre(), producto.getCantidadXUnidad(), producto.getPrecio());
	}

	public ProductoResponse(Producto producto, String message) {
		this(producto.getNombre(), producto.getCantidadXUnidad(), producto.getPrecio());
		addMessage(message);
	}

}
