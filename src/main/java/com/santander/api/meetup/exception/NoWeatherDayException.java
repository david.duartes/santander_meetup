package com.santander.api.meetup.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NoWeatherDayException extends Exception {

	private static final long serialVersionUID = 1L;

		
}
