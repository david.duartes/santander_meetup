package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Rol;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RolResponse extends SuccessResponse {

	private String tipo;

	public RolResponse(Rol rol) {
		this(rol.getTipo());
	}

	public RolResponse(Rol rol, String message) {
		this(rol.getTipo());
		addMessage(message);
	}

}
