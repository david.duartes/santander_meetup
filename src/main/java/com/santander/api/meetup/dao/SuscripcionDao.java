package com.santander.api.meetup.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Suscripcion;

@Repository
public interface SuscripcionDao extends CrudRepository<Suscripcion, Long> {
	
	 List<Suscripcion> findByMeetup(Meetup meetupId);

}
