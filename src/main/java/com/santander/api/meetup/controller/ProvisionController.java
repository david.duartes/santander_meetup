package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.ProvisionResponse;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.ProvisionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/provisiones")
public class ProvisionController {

	@Autowired
	private ProvisionService provisionService;

	@PostMapping
	@ApiOperation(value = "Dar de alta una provisión para una meetup")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Provision creada correctamente", response = ProvisionResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearProvision(
			@ApiParam(value = "Datos del provisión para la meetup", required = true) @Valid @RequestBody Provision provision) {

		Provision provisionCreated = provisionService.save(provision);

		return new ResponseEntity<GenericResponse>(
				new ProvisionResponse(provisionCreated, "Provision creada correctamente"), HttpStatus.CREATED);

	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener provision por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = ProvisionResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getProvisionById(@Valid @PathVariable("id") Long id) {

		Provision provision = provisionService.get(id);

		if (provision == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Provisión no encontrada"),
					HttpStatus.NOT_FOUND);

		}

		return new ResponseEntity<GenericResponse>(new ProvisionResponse(provision, "Ok"), HttpStatus.OK);

	}

}
