package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UsuarioResponse extends SuccessResponse {

	private String userName;

	private String password;

	private Rol rol;

	private boolean activo;

	public UsuarioResponse(Usuario usuario) {
		this(usuario.getUserName(), usuario.getPassword(), usuario.getRol(), usuario.isActivo());
	}

	public UsuarioResponse(Usuario usuario, String message) {
		this(usuario.getUserName(), usuario.getPassword(), usuario.getRol(), usuario.isActivo());
		addMessage(message);
	}

}
