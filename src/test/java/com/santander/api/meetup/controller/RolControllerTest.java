package com.santander.api.meetup.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.RolDao;
import com.santander.api.meetup.enums.TipoRolEnum;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.service.RolService;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class RolControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private RolService rolService;

	@Autowired
	@Getter
	private RolDao RolDao;

//	@Test
	public void crearRolTest() throws JsonProcessingException, Exception {

		Rol request = new Rol(null, TipoRolEnum.ADMIN.name());
		this.mockMvc
				.perform(post("/v1/roles").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated()).andExpect(content().string(containsString(
						"{\"success\":true,\"messages\":[\"Rol creado correctamente\"],\"tipo\":\"ADMIN\"}")));
	}

}
