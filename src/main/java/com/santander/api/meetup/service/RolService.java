package com.santander.api.meetup.service;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.model.Rol;

public interface RolService extends GenericService<Rol, Long> {

}
