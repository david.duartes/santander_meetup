package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.RolDao;
import com.santander.api.meetup.enums.TipoRolEnum;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.service.impl.RolServiceImpl;

@ExtendWith(MockitoExtension.class)
public class RolServiceTests {

	private static final Rol rolPrueba = new Rol(1L, TipoRolEnum.ADMIN.name());

	@Mock
	private RolDao rolDao;

	@InjectMocks
	private RolServiceImpl rolServiceImpl;

//	@BeforeEach
//    void setMockOutput() {
//        when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//    }

	@Test
	public void testSaveSuccess() {

		Rol input = new Rol(null, TipoRolEnum.ADMIN.name());

		when(rolDao.save(input)).thenReturn(rolPrueba);

		assertEquals(rolPrueba, rolServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long idPrueba = 1L;

		Optional<Rol> uOpResult = Optional.of(rolPrueba);

		when(rolDao.findById(idPrueba)).thenReturn(uOpResult);

		assertEquals(rolPrueba, rolServiceImpl.get(idPrueba));
	}

}
