package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.ProductoDao;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.service.ProductoService;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private ProductoService productoService;

	@Autowired
	@Getter
	private ProductoDao productoDao;

//	@Test
	public void crearProductoTest() throws JsonProcessingException, Exception {

		String nombre = "Pack Cerveza";

		int cantidadXUnidad = 6;

		Double precio = Double.valueOf(760);

		Long id = null;

		Producto request = new Producto(id, nombre, cantidadXUnidad, precio);

		this.mockMvc
				.perform(post("/v1/productos").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated());
	}

//	@Test
//	@Order(4)
	public void getProductoByIdTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/productos/" + id)).andDo(print()).andExpect(status().isOk());
//				.andExpect(content().string(containsString("Hello, World")));
	}

}
