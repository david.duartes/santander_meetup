package com.santander.api.meetup.service;

import com.santander.api.meetup.model.weather.Weather;

public interface OpenWeatherService {

	public Weather getDaily();

}
