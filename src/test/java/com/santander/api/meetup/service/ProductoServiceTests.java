package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.ProductoDao;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.service.impl.ProductoServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ProductoServiceTests {

	private static final Producto productoPrueba = new Producto(10L, "Pack cerveza", 6, 769.0);

	@Mock
	private ProductoDao productoDao;

	@InjectMocks
	private ProductoServiceImpl productoServiceImpl;

//	@BeforeEach
//    void setMockOutput() {
//        when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//    }

	@Test
	public void testSaveSuccess() {

		Producto input = new Producto(null, "Pack cerveza", 6, 769.0);

		when(productoDao.save(input)).thenReturn(productoPrueba);

		assertEquals(productoPrueba, productoServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long idPrueba = 1L;

		Optional<Producto> uOpResult = Optional.of(productoPrueba);

		when(productoDao.findById(idPrueba)).thenReturn(uOpResult);

		assertEquals(productoPrueba, productoServiceImpl.get(idPrueba));
	}

}
