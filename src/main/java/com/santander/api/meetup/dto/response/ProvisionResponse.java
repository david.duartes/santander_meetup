package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.model.Provision;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProvisionResponse extends SuccessResponse {

	private int cantidad;

	public ProvisionResponse(Provision provision) {
		this(provision.getCantidad());
	}

	public ProvisionResponse(Provision provision, String message) {
		this(provision.getCantidad());
		addMessage(message);
	}

}
