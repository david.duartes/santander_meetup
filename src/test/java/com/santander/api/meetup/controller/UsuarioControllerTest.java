package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.UsuarioDao;
import com.santander.api.meetup.enums.TipoRolEnum;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.UsuarioService;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private UsuarioService usuarioService;

	@Autowired
	@Getter
	private UsuarioDao usuarioDao;

//	@Test
	public void crearUsuarioTest() throws JsonProcessingException, Exception {

		Rol rol = new Rol(1L, TipoRolEnum.ADMIN.name());
		Usuario request = new Usuario(null, "David", "1234", rol, true);
		this.mockMvc
				.perform(post("/v1/usuarios").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated());
//				.andExpect(content().string(containsString("Hello, World")));
	}

}
