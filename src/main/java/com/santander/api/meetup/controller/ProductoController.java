package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.ProductoResponse;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.service.ProductoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/productos")
public class ProductoController {

	@Autowired
	private ProductoService productoService;

	@PostMapping
	@ApiOperation(value = "Dar de alta un producto")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Producto creado correctamente", response = ProductoResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearMeetup(
			@ApiParam(value = "Datos del producto a crear en el sistema", required = true) @Valid @RequestBody Producto producto) {
		Producto productoCreated = productoService.save(producto);

		if (productoCreated == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("No se ha podido crear el producto"),
					HttpStatus.NOT_IMPLEMENTED);
		}

		return new ResponseEntity<GenericResponse>(
				new ProductoResponse(productoCreated, "Producto creado correctamente"), HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener producto por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = ProductoResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getProductoById(@Valid @PathVariable("id") Long id) {

		Producto producto = productoService.get(id);

		if (producto == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Producto no encontrado"),
					HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<GenericResponse>(new ProductoResponse(producto, "Ok"), HttpStatus.OK);

	}
}
