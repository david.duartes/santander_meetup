package com.santander.api.meetup.dto.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class MeetupDto {

	private String nombre;

	private Date fecha;

	private String lugar;

	private String descripcion;

}
