package com.santander.api.meetup.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.santander.api.meetup.model.Producto;

@Repository
public interface ProductoDao extends CrudRepository<Producto, Long> {

}
