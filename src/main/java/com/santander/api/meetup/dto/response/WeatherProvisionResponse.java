package com.santander.api.meetup.dto.response;

import com.santander.api.meetup.dto.MeetupProvisionDTO;
import com.santander.api.meetup.model.Meetup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class WeatherProvisionResponse extends SuccessResponse {

	private Meetup meetup;

	private double cantidadAComprar;

	public WeatherProvisionResponse(MeetupProvisionDTO meetupProvisionDTO) {
		this(meetupProvisionDTO.getMeetup(), meetupProvisionDTO.getCantidadAComprar());
	}

	public WeatherProvisionResponse(MeetupProvisionDTO meetupProvisionDTO, String message) {
		this(meetupProvisionDTO.getMeetup(), meetupProvisionDTO.getCantidadAComprar());
		addMessage(message);
	}

}
