package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.santander.api.meetup.model.weather.Weather;
import com.santander.api.meetup.service.OpenWeatherService;

@Service
public class OpenWeatherServiceImpl implements OpenWeatherService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${openWeather.rest.url}")
	private String restUrl;

	@Override
	public Weather getDaily() {

		return restTemplate.getForObject(restUrl, Weather.class);

	}

}
