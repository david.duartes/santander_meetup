package com.santander.api.meetup.facade;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.dto.MeetupProvisionDTO;
import com.santander.api.meetup.exception.NoWeatherDayException;
import com.santander.api.meetup.exception.NotFoundMeetupException;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.model.Suscripcion;
import com.santander.api.meetup.model.weather.Day;
import com.santander.api.meetup.model.weather.Weather;
import com.santander.api.meetup.service.MeetupService;
import com.santander.api.meetup.service.OpenWeatherService;
import com.santander.api.meetup.service.SuscripcionService;

@Service
public class MeetupInfoFacade {

	@Autowired
	private MeetupService meetupService;

	@Autowired
	private SuscripcionService suscripcionService;

	@Autowired
	private OpenWeatherService openWeatherService;

	private final static double MAX_TEMP = 24;

	private final static double MIN_TEMP = 20;

	private final static double MIN_CANT = 0.72;

	private final static double NORMAL_CANT = 1;

	private final static double MAX_CANT = 2;

	public Day obtenerTemperatura(Long id) throws NoWeatherDayException, NotFoundMeetupException {

		Weather result = null;

		Day dia = null;

		if (id != null) {

			Meetup meetup = meetupService.get(id);

			if (meetup != null) {

				result = openWeatherService.getDaily();

				dia = buscarTemperaturaMeetup(result.getList(), meetup);

				if (dia == null) {

					throw new NoWeatherDayException();
				}

				return dia;
			}

		}

		throw new NotFoundMeetupException();

	}

	public MeetupProvisionDTO calcularProvision(Long id) throws NoWeatherDayException, NotFoundMeetupException {

		Weather result = null;

		if (id != null) {

			Meetup meetup = meetupService.get(id);

			if (meetup != null) {

				result = openWeatherService.getDaily();

				Day dia = buscarTemperaturaMeetup(result.getList(), meetup);

				if (dia == null) {

					throw new NoWeatherDayException();
				}

				return calcularCantidadCervezas(meetup, dia);

			}
		}

		throw new NotFoundMeetupException();
	}

	private Day buscarTemperaturaMeetup(List<Day> dias, Meetup meetup) {

		DateFormat df = new SimpleDateFormat("dd/MM/yy");

		Date meetupDate = meetup.getFecha();

		for (Day dia : dias) {

			Date weatherDate = Date.from(dia.getDt());

			String weatherDateString = df.format(weatherDate);

			String meetupDateString = df.format(meetupDate);

			if (weatherDateString.equalsIgnoreCase(meetupDateString)) {

				return dia;
			}

		}

		return null;

	}

	private MeetupProvisionDTO calcularCantidadCervezas(Meetup meetup, Day d) {

		MeetupProvisionDTO meetupProvisionDTO = null;

		// Obtengo suscripciones para tener cantidad de participantes
		List<Suscripcion> suscripciones = suscripcionService.getByIdMeetup(meetup);

		if (suscripciones != null) {

			// Obtengo provision para tener el id de producto
			Provision provision = meetup.getProvision();

			// Obtengo el producto para tener la cantidad por unidad
			Producto producto = provision.getProducto();

			int cantSuscripciones = suscripciones.size();

			Double cantidadProducto = Double.valueOf(NORMAL_CANT);

			Double max = d.getTemp().getMax();

			if (max < MIN_TEMP) {

				cantidadProducto = Double.valueOf(MIN_CANT);

			}

			if (max > MAX_TEMP) {

				cantidadProducto = Double.valueOf(MAX_CANT);

			}

			double cantidadAComprar = Math.ceil((cantSuscripciones * cantidadProducto) / producto.getCantidadXUnidad());

			// Completo datos de la meetup
			meetupProvisionDTO = new MeetupProvisionDTO(meetup, cantidadAComprar);

		}
		return meetupProvisionDTO;
	}

}
