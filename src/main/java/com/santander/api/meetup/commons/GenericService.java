package com.santander.api.meetup.commons;

import java.io.Serializable;

public interface GenericService<T, ID extends Serializable> {

	T save(T entity);

	void delete(ID id);

	T get(ID id);

}
