package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.SuscripcionDao;
import com.santander.api.meetup.enums.TipoRolEnum;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.model.Suscripcion;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.impl.SuscripcionServiceImpl;

@ExtendWith(MockitoExtension.class)
public class SuscripcionServiceTests {

	private static final Rol rolPrueba = new Rol(1L, TipoRolEnum.ADMIN.name());

	private static final Usuario usuario = new Usuario(1L, "prueba", "1234", rolPrueba, true);

	private static final Provision provisionPrueba = new Provision(1L);

	private static final Meetup meetupPrueba = new Meetup(1L, "Meetup Prueba", new Date(), "Sala 1", "Reunión After",
			provisionPrueba);

	private static final Suscripcion suscripcionPrueba = new Suscripcion(10L, usuario, meetupPrueba);

	@Mock
	private SuscripcionDao suscripcionDao;

	@InjectMocks
	private SuscripcionServiceImpl suscripcionServiceImpl;

//	@BeforeEach
//    void setMockOutput() {
//        when(helloRepository.get()).thenReturn("Hello Mockito From Repository");
//    }

	@Test
	public void testSaveSuccess() {

		Suscripcion input = new Suscripcion(null, usuario, meetupPrueba);

		when(suscripcionDao.save(input)).thenReturn(suscripcionPrueba);

		assertEquals(suscripcionPrueba, suscripcionServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long idPrueba = 1L;

		Optional<Suscripcion> uOpResult = Optional.of(suscripcionPrueba);

		when(suscripcionDao.findById(idPrueba)).thenReturn(uOpResult);

		assertEquals(suscripcionPrueba, suscripcionServiceImpl.get(idPrueba));
	}

}
