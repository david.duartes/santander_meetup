package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.ProvisionDao;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.ProvisionService;

@Service
public class ProvisionServiceImpl extends GenericServiceImpl<Provision, Long> implements ProvisionService {

	@Autowired
	private ProvisionDao provisionDao;

	@Override
	public CrudRepository<Provision, Long> getDao() {
		return provisionDao;
	}

}
