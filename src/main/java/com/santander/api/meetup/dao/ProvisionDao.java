package com.santander.api.meetup.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.santander.api.meetup.model.Provision;

@Repository
public interface ProvisionDao extends CrudRepository<Provision, Long> {

}
