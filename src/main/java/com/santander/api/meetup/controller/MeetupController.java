package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.MeetupProvisionDTO;
import com.santander.api.meetup.dto.response.ErrorResponse;
import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.MeetupResponse;
import com.santander.api.meetup.dto.response.WeatherProvisionResponse;
import com.santander.api.meetup.dto.response.WeatherResponse;
import com.santander.api.meetup.exception.NoWeatherDayException;
import com.santander.api.meetup.exception.NotFoundMeetupException;
import com.santander.api.meetup.facade.MeetupInfoFacade;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.weather.Day;
import com.santander.api.meetup.service.MeetupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/meetups")
public class MeetupController {

	@Autowired
	private MeetupService meetupService;

	@Autowired
	private MeetupInfoFacade meetupInfoFacade;

	@PostMapping
	@ApiOperation(value = "Crear un meetup")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Meetup creado correctamente", response = MeetupResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearMeetup(
			@ApiParam(value = "Datos del meetup a crear en el sistema", required = true) @Valid @RequestBody Meetup meetup) {

		Meetup meetupCreated = meetupService.save(meetup);

		return new ResponseEntity<GenericResponse>(new MeetupResponse(meetupCreated, "Meetup creado correctamente"),
				HttpStatus.CREATED);

	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener meetup por id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = MeetupResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getMeetupById(@Valid @PathVariable("id") Long id) {

		Meetup meetup = meetupService.get(id);

		if (meetup == null) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Meetup no encontrada"), HttpStatus.NOT_FOUND);

		}

		return new ResponseEntity<GenericResponse>(new MeetupResponse(meetup, "Ok"), HttpStatus.OK);
	}

	@GetMapping("/{id}/temperaturas/info")
	@ApiOperation(value = "Obtener la temperatura pronosticada para el día de la meetup")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = WeatherResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "No se encontraron datos de clima para la fecha de meetup indicada.", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "No se encontro meetup buscada", response = ErrorResponse.class),
			@ApiResponse(code = 500, message = "Se produjo un error no esperado", response = ErrorResponse.class) })
	public ResponseEntity<GenericResponse> obtenerTemperatura(@Valid @PathVariable("id") Long id) {

		Day day;

		try {

			day = meetupInfoFacade.obtenerTemperatura(id);

		} catch (NoWeatherDayException e) {

			return new ResponseEntity<GenericResponse>(
					new ErrorResponse("No se encontraron datos de clima para la fecha de meetup indicada."),
					HttpStatus.NOT_FOUND);

		} catch (NotFoundMeetupException e) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("No se encontro meetup buscada"),
					HttpStatus.NOT_FOUND);

		} catch (Exception e) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Se produjo un error no esperado"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<GenericResponse>(new WeatherResponse(day, "Ok"), HttpStatus.OK);
	}

	@GetMapping("/{id}/provisiones/info")
	@ApiOperation(value = "Calcula la cantidad de provisiones a comprar para la meetup")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = WeatherProvisionResponse.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "No se encontraron datos de clima para la fecha de meetup indicada.", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "No se encontro meetup buscada", response = ErrorResponse.class),
			@ApiResponse(code = 500, message = "Se produjo un error no esperado", response = ErrorResponse.class) })
	public ResponseEntity<GenericResponse> calcularProvision(@Valid @PathVariable("id") Long id) {

		MeetupProvisionDTO meetupProvisionDTO = null;

		try {

			meetupProvisionDTO = meetupInfoFacade.calcularProvision(id);

		} catch (NoWeatherDayException e) {

			return new ResponseEntity<GenericResponse>(
					new ErrorResponse("No se encontraron datos de clima para la fecha de meetup indicada."),
					HttpStatus.NOT_FOUND);

		} catch (NotFoundMeetupException e) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("No se encontro meetup buscada"),
					HttpStatus.NOT_FOUND);

		} catch (Exception e) {

			return new ResponseEntity<GenericResponse>(new ErrorResponse("Se produjo un error no esperado"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<GenericResponse>(new WeatherProvisionResponse(meetupProvisionDTO, "Ok"),
				HttpStatus.OK);
	}
}
