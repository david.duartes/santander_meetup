package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.RolDao;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.service.RolService;

@Service
public class RolServiceImpl extends GenericServiceImpl<Rol, Long> implements RolService {

	@Autowired
	private RolDao rolDao;

	@Override
	public CrudRepository<Rol, Long> getDao() {
		return rolDao;
	}

}
