package com.santander.api.meetup.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public abstract class GenericResponse {

	private boolean success;
	private List<String> messages;

	protected GenericResponse(boolean success) {
		this.success = success;
		this.messages = new ArrayList<String>();
	}

	protected void addMessage(String message) {
		if (message == null) {
			return;
		}

		if (this.messages == null) {
			this.messages = new ArrayList<String>();
		}

		this.messages.add(message);
	}

}
