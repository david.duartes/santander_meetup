package com.santander.api.meetup.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.santander.api.meetup.dao.MeetupDao;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.impl.MeetupServiceImpl;

@ExtendWith(MockitoExtension.class)
public class MeetupServiceTests {

	private static final Provision provisionPrueba = new Provision(1L);

	private static final Meetup meetupPrueba = new Meetup(1L, "Meetup Prueba", new Date(), "Sala 1", "Reunión After",
			provisionPrueba);

	@Mock
	private MeetupDao meetupRepository;

	@InjectMocks
	private MeetupServiceImpl meetupServiceImpl;

	@Test
	public void testSaveSuccess() {

		Meetup input = new Meetup(null, "Meetup Prueba", new Date(), "Sala 1", "Reunión After", provisionPrueba);

		when(meetupRepository.save(input)).thenReturn(meetupPrueba);

		assertEquals(meetupPrueba, meetupServiceImpl.save(input));

	}

	@Test
	public void testFindById() {

		Long id = 1L;

		Optional<Meetup> optional = Optional.of(meetupPrueba);

		when(meetupRepository.findById(id)).thenReturn(optional);

		assertEquals(meetupPrueba, meetupServiceImpl.get(id));

	}

}
