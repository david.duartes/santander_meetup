package com.santander.api.meetup.service;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.model.Provision;

public interface ProvisionService extends GenericService<Provision, Long> {

}
