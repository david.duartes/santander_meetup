package com.santander.api.meetup.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Table(name = "usuarios")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@ApiModel(description = "Clase que representa usuario de meetups.")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	@ApiModelProperty(notes = "Nombre del usuario", example = "prueba", required = true, position = 0)
	@Column(unique = true)
	private String userName;

	@NonNull
	@ApiModelProperty(notes = "Password del usuario", example = "1234", required = true, position = 1)
	private String password;

	@NonNull
	@OneToOne
	@ApiModelProperty(notes = "Rol que va a tener el usuario (1-ADMIN/2-USER)", required = true, position = 2)
	private Rol rol;

	private boolean activo;

	public Usuario(Long id) {
		super();
		this.id = id;
	}

}
