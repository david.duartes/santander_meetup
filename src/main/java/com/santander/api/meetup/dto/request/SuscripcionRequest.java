package com.santander.api.meetup.dto.request;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class SuscripcionRequest {

	private Long usuarioId;

	private Long meetupId;
}
