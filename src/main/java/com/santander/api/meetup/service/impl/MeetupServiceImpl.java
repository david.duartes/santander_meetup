package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.MeetupDao;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.service.MeetupService;

@Service
public class MeetupServiceImpl extends GenericServiceImpl<Meetup, Long> implements MeetupService {

	@Autowired
	private MeetupDao meetupDao;

	@Override
	public CrudRepository<Meetup, Long> getDao() {
		return meetupDao;
	}

}
