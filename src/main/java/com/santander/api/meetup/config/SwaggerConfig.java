package com.santander.api.meetup.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	/**
	 * Create Swagger Api configuration
	 *
	 * @return Swagger Docket
	 */
	@Bean
	public Docket api() {
		
		final String swaggerToken = "";
		
		return new Docket(DocumentationType.SWAGGER_2).groupName("santander")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.paths(PathSelectors.any()).build()
				.pathMapping("/")
				.genericModelSubstitutes(ResponseEntity.class)
				.useDefaultResponseMessages(false)
				.globalOperationParameters(Collections.singletonList(
	                    new ParameterBuilder()
	                            .name("Authorization")
	                            .modelRef(new ModelRef("string"))
	                            .parameterType("header")
	                            .required(true)
	                            .hidden(true)
	                            .defaultValue("Bearer " + swaggerToken)
	                            .build()));
	}


	/**
	 * Generate Api Info
	 *
	 * @return Swagger API Info
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Santander Meetup API").description("Challenge").version("0.1-SNAPSHOT")
				.license("URL Repositorio del proyecto").licenseUrl("https://gitlab.com/david.duartes/santander_meetup")
				.build();
	}
}
