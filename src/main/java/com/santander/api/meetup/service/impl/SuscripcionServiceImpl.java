package com.santander.api.meetup.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.SuscripcionDao;
import com.santander.api.meetup.dto.request.SuscripcionRequest;
import com.santander.api.meetup.exception.NotFoundMeetupException;
import com.santander.api.meetup.exception.NotFoundUsuarioException;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Suscripcion;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.MeetupService;
import com.santander.api.meetup.service.SuscripcionService;
import com.santander.api.meetup.service.UsuarioService;

@Service
public class SuscripcionServiceImpl extends GenericServiceImpl<Suscripcion, Long> implements SuscripcionService {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private MeetupService meetupService;

	@Autowired
	private SuscripcionDao suscripcionDao;

	@Override
	public CrudRepository<Suscripcion, Long> getDao() {
		return suscripcionDao;
	}

	@Override
	public Suscripcion crearSuscripcion(SuscripcionRequest request)
			throws NotFoundMeetupException, NotFoundUsuarioException {

		Suscripcion suscripcionResponse = null;

		if (request != null && request.getMeetupId() != null && request.getUsuarioId() != null) {

			Usuario usuario = usuarioService.get(request.getUsuarioId());

			if (usuario == null) {
				throw new NotFoundUsuarioException();
			}

			Meetup meetup = meetupService.get(request.getMeetupId());

			if (meetup == null) {
				throw new NotFoundMeetupException();
			}

			Suscripcion suscripcion = new Suscripcion(null, usuario, meetup);

			suscripcionResponse = suscripcionDao.save(suscripcion);

		}

		return suscripcionResponse;
	}

	@Override
	public List<Suscripcion> getByIdMeetup(Meetup meetup) {

		return suscripcionDao.findByMeetup(meetup);

	}

}
