package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.SuscripcionDao;
import com.santander.api.meetup.dto.request.SuscripcionRequest;
import com.santander.api.meetup.service.SuscripcionService;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class SuscripcionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private SuscripcionService suscripcionService;

	@Autowired
	@Getter
	private SuscripcionDao suscripcionDao;

//	@Test
	public void crearSuscripcionTest() throws Exception {

		Long usuarioId = 1L;

		Long meetupId = 1L;

		SuscripcionRequest request = new SuscripcionRequest(usuarioId, meetupId);

		this.mockMvc
				.perform(post("/v1/suscripciones").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated());
//				.andExpect(content().string(containsString("Hello, World")));
	}

	// @Test
//	@Order(10)
	public void getSuscripcionById() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/suscripciones/" + id)).andDo(print()).andExpect(status().isOk());
	}

	// @Test
//	@Order(10)
	public void getByIdMeetup() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/suscripciones/" + id)).andDo(print()).andExpect(status().isOk());
	}

}
