package com.santander.api.meetup.service;

import java.util.List;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.dto.request.SuscripcionRequest;
import com.santander.api.meetup.exception.NotFoundMeetupException;
import com.santander.api.meetup.exception.NotFoundUsuarioException;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Suscripcion;

public interface SuscripcionService extends GenericService<Suscripcion, Long> {

	public Suscripcion crearSuscripcion(SuscripcionRequest request)
			throws NotFoundMeetupException, NotFoundUsuarioException;

	public List<Suscripcion> getByIdMeetup(Meetup meetup);

}
