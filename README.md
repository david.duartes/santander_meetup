# Santander Meetup API Rest

Esta API Rest se propone ofrecer las funcionalidades necesarias para la creación, administración, cálculo de provisiones y temperatura de Meetups.

## Arquitectura general

La API está desarrollada en Java con Spring Boot como base de estructura de la misma y utilizando formato Json tanto para los request como para response.

En la parte de seguridad esta API utiliza Spring Security con JWT, para poder autenticar y acceder a los recursos por medio de token.

Para el acceso a datos se utiliza JPA, el mismo incluido dentro de Spring Boot.

### Base de datos
La base de datos utilizada es MySql versión 8 y la misma se encuentra corriendo en una instancia RDS de AWS.

### Autentificación Token
Para autenticar y obtener un token se debe realizar un llamado post a:

http://ec2-18-229-124-2.sa-east-1.compute.amazonaws.com:8080/login

enviando en el encabezado el usuario y password siguiente:

user:admin
user:admin

o cualquier usuario registrado.

### Implementación
La API se encuentra corriendo en un contenedor docker en un servidor C2E de AWS en la siguiente URL:

[http://ec2-18-229-124-2.sa-east-1.compute.amazonaws.com](http://ec2-18-229-124-2.sa-east-1.compute.amazonaws.com)

escuchando en el puerto 8080 por defecto de Spring Boot.

### Deploy Automático
Los deploy en dichos contenedores y servidores AWS están automatizados por medio de la herramienta que provee Gitlab CI a través de la configuración de un Runner. Esta configuración está especificada en el archivo YAML llamado ".gitlab-ci.yml"

### Test unitarios
Los test unitarios está desarrollados con JUnit 5, junto con la utilización de la librería Mockito para realizar los test de cada módulo.

## Swagger documentación API
Tanto los endpoints como la estructura de los mismos, se encuentran documentados a traves de la herramienta Swagger.

URL Swagger UI:

[Santander Meetup API](http://ec2-18-229-124-2.sa-east-1.compute.amazonaws.com:8080/swagger-ui.html)

## Diagramas de la aplicación

[Diagrama de clases](https://drive.google.com/file/d/1ZXd6si0j-_5XdH_fV6v4RqXD7BWcsKEa/view?usp=sharing)


[Diagrama de casos de uso](https://drive.google.com/file/d/1MxIC0TPCYJVH0E2fPJxBRIrOB1a_SSe5/view?usp=sharing)