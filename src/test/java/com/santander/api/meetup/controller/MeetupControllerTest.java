package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.MeetupDao;
import com.santander.api.meetup.model.Meetup;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.impl.MeetupServiceImpl;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class MeetupControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private MeetupServiceImpl meetupService;

	@Autowired
	@Getter
	private MeetupDao meetupDao;

//	@Test
	public void crearMeetupTest() throws Exception {

		Long id = 1L;

		Provision provision = new Provision(id);

		Meetup request = new Meetup(null, "Prueba", new Date(), "Home", "Prueba Home", provision);
		this.mockMvc
				.perform(post("/v1/meetups").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated());
//				.andExpect(content().string(containsString("Hello, World")));
	}

//	@Test
//	@Order(8)
	public void getMeetupByIdTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/meetups/" + id)).andDo(print()).andExpect(status().isOk());
//				.andExpect(content().string(containsString("Hello, World")));
	}

}
