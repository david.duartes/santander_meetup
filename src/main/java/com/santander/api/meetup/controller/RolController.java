package com.santander.api.meetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.santander.api.meetup.dto.response.GenericResponse;
import com.santander.api.meetup.dto.response.RolResponse;
import com.santander.api.meetup.model.Rol;
import com.santander.api.meetup.service.RolService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/v1/roles")
public class RolController {

	@Autowired
	private RolService rolService;

	@PostMapping
	@ApiOperation(value = "Crear rol de usuario")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 201, message = "Rol creado correctamente", response = RolResponse.class), @ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> crearRol(
			@ApiParam(value = "Datos del rol a crear en el sistema", required = true)
			@Valid @RequestBody Rol rol) {
		
		Rol rolCreated = rolService.save(rol);
		
		return new ResponseEntity<GenericResponse>(new RolResponse(rolCreated, "Rol creado correctamente"),
				HttpStatus.CREATED);
		
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Obtener Rol por id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = RolResponse.class), @ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Failure") })
	public ResponseEntity<GenericResponse> getRolyId(
			@Valid @PathVariable("id") Long id) {
		
		Rol rol = rolService.get(id);
		
		return new ResponseEntity<GenericResponse>(new RolResponse(rol, "Ok"), HttpStatus.OK);
		
	}

}
