package com.santander.api.meetup.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.api.meetup.dao.ProvisionDao;
import com.santander.api.meetup.model.Producto;
import com.santander.api.meetup.model.Provision;
import com.santander.api.meetup.service.ProvisionService;

import lombok.Getter;

@SpringBootTest
@AutoConfigureMockMvc
public class ProvicionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	@Getter
	private ProvisionService ProvisionService;

	@Autowired
	@Getter
	private ProvisionDao provisionDao;

//	@Test
	public void crearProvicionTest() throws Exception {

		Long id = 1L;

		Producto producto = new Producto(id);

		Provision request = new Provision(null, 10, producto);

		this.mockMvc
				.perform(post("/v1/provisiones").contentType("application/json")
						.content(objectMapper.writeValueAsString(request)))
				.andDo(print()).andExpect(status().isCreated());
//				.andExpect(content().string(containsString("Hello, World")));
	}

//	@Order(6)
//	@Test
	public void getProvisionByIdTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(get("/v1/provisiones/" + id)).andDo(print()).andExpect(status().isOk());
//				.andExpect(content().string(containsString("Hello, World")));
	}

}
