package com.santander.api.meetup.service;

import com.santander.api.meetup.commons.GenericService;
import com.santander.api.meetup.model.Producto;

public interface ProductoService extends GenericService<Producto, Long> {

}
