package com.santander.api.meetup.dto.response;

import java.util.Date;

import com.santander.api.meetup.model.Meetup;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MeetupResponse extends SuccessResponse {

	private String nombre;

	private Date fecha;

	private String lugar;

	private String descripcion;

	public MeetupResponse(Meetup meetup) {
		this(meetup.getNombre(), meetup.getFecha(), meetup.getLugar(), meetup.getDescripcion());
	}

	public MeetupResponse(Meetup meetup, String message) {
		this(meetup.getNombre(), meetup.getFecha(), meetup.getLugar(), meetup.getDescripcion());
		addMessage(message);
	}

}
