package com.santander.api.meetup.dto.request;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class UsuarioRequest {

	private String userName;

	private String password;
	
	private Long rolId;
}
