package com.santander.api.meetup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.santander.api.meetup.commons.impl.GenericServiceImpl;
import com.santander.api.meetup.dao.UsuarioDao;
import com.santander.api.meetup.model.Usuario;
import com.santander.api.meetup.service.UsuarioService;

@Service
public class UsuarioServiceImpl extends GenericServiceImpl<Usuario, Long> implements UsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	public CrudRepository<Usuario, Long> getDao() {
		return usuarioDao;
	}

	@Override
	public Usuario findByUserName(String userName) {
		return usuarioDao.findByUserName(userName);
	}

}
